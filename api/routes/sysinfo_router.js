// - - - - - - - - - - - - - - - - projects Router - - - - - - - - - - - - - - - - -  //                                                                                 */
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

'use strict';

const Router = require('koa-router');
const sysInfoRouter = new Router;

const sysInfoCTRL = require('../controllers/sysinfo_ctrl');

// get info
sysInfoRouter.get('/sysinfo', 
    sysInfoCTRL.getInfo
);

module.exports = sysInfoRouter;