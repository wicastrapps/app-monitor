'use strict';

const promisify = require("promisify-node");
const si = promisify(require('systeminformation'));
const moment = require('moment');
const momentDuration = require("moment-duration-format");
const exec = require('child-process-promise').exec;

/**
 * SysInfoController is module retrieving system information
 *
 * @example
 * var sysInfoController = require('SysInfoController');
 * sysinfoController.getInfo()
 *
 * @namespace module:controllers~sysInfoController
 * @type {Object}
 * @version v0
 * @variation v0
 */

var controller  = {}
module.exports = controller;


/**
 * SysInfoController.getInfo
 *
 * @param {object}  this                        - ctx object of KOA which contains request data.
 *
 * @desc Retrieves system information
 *
 * @alias sysInfoController.getInfo
 * @memberOf! module:controllers~sysInfoController(v0)
 * @public
 */
controller.getInfo = function*() {
    let data = {};

    let cpuLoad = yield si.currentLoad();
    cpuLoad.currentload = Math.round(cpuLoad.currentload * 100) / 100;
    cpuLoad.avgload = Math.round(cpuLoad.avgload * 100) / 100;

    data.cpuLoad = cpuLoad;
    data.proc = [];

    let proc = yield si.services('node, chromium, X, nginx');
    for (let i = 0; i < proc.length; i ++) {
        data.proc.push(proc[i]);
    }

    let time = yield si.time();

    time.uptime = moment.duration(time.uptime * 1000).format('d[d] H:mm:ss');
    data.time = time;

    let mem = yield si.mem();
    mem.total = Math.round((mem.total / (1024 * 1024)) * 100) / 100;
    mem.free = Math.round((mem.free / (1024 * 1024)) * 100) / 100;
    mem.used = Math.round((mem.used / (1024 * 1024)) * 100) / 100;

    data.mem = mem;


    let ltnc = yield si.inetLatency();
    let inetTxt = isNaN(ltnc) ? "disconnected" : "connected";
    let inetStatus = !isNaN(ltnc) ? "success" : "danger";

    data.inet = {
        'txt': inetTxt,
        'status': inetStatus
    };

    let accel = {
        'txt': "off",
        'status': "danger"
    };

    try {
        let directRendering = yield exec('DISPLAY=:0 glxinfo | grep "direct rendering"');
        if (directRendering.stdout && directRendering.stdout.indexOf("Yes") !== -1) {
            accel.txt = "on";
            accel.status = "success";
        }
    } catch (err) {
        accel.txt = "unknown";
        accel.status = "danger";
    }

    data.gpu = {
        'accel': accel
    }

    this.status = 202;
    this.body = {
        'info': data
    };
};

