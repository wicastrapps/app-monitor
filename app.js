/**
 * @module app
 */

'use strict';

/** Create KOA app */
var koa = require('koa');
var app = koa();


const Router        = require('koa-router')();        /** @const  Router       Middleware for routing. */
const BodyParser    = require('koa-body');          /** @const  BodyParser   Middleware for accessing json. */
const Serve         = require('koa-static');        /** @const  Serve        Middleware for serving files. */
const Logger        = require('koa-logger');        /** @const  Logger       Middleware for logging. */
const ResponseTime  = require('koa-response-time'); /** @const  ResponseTime X-Response-Time middleware. */

const path = require('path');

const args = process.argv;


// ---------- Connect Middlewares ---------- //
app.use(ResponseTime());
app.use(Logger());
app.use(Serve(path.join(__dirname, '/app/dist')));
app.use(BodyParser());

app.use(Router.routes());
app.use(Router.allowedMethods());

// -------------- Set Header -------------- //
app.use(function *(next) {
    this.set('Access-Control-Allow-Origin', '*');
    this.set('Access-Control-Allow-Methods', 'HEAD, GET, OPTIONS');
    this.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    if (this.method === 'OPTIONS') {
      this.status = 204;
    } else {
      yield next;
    }
});

// ------------ Load Routers ------------ //
var monitorRouter = require('./app/routes/monitor_router');
var sysInfoRouter = require('./api/routes/sysinfo_router');

// ---------- Connect Routers ---------- //
app.use(monitorRouter.routes());
app.use(sysInfoRouter.routes());

app.use(function *() {
    this.status = 404;
    this.body = 'Invalid URL!';
});

// Port can specified as an argument when invoking this app via pm2 as:
// e.g. pm2 start app.js -- p 3001
//args.forEach((x, y)=>{
//    if (x == "p") {
//        port = args[y + 1];
//    }
//});

const port = process.env.NODE_PORT || 8000
app.listen(port)

console.log('Server start ' + port)