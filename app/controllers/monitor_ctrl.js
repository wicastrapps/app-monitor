'use strict';

const path = require('path');

const Views = require('co-views');
const Render = Views(path.join(path.dirname(__dirname), '/dist/views'), {ext: 'ejs'});

/**
 * MonitorController is module delivering system information to the user
 *
 * @example
 * var monitorController = require('MonitorController');
 * monitorController.getInfo()
 *
 * @namespace module:controllers~monitorController
 * @type {Object}
 * @version v0
 * @variation v0
 */

var controller  = {}
module.exports = controller;


/**
 * MonitorController.displayInfo
 *
 * @param {object}  this                        - ctx object of KOA which contains request data.
 *
 * @desc Displays system information
 *
 * @alias monitorController.displayInfo
 * @memberOf! module:controllers~monitorController(v0)
 * @public
 */
controller.displayInfo = function*() {
    this.body = yield Render('monitor');
};

