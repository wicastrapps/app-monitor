
# WiCastr API Starter controllers#

***
### Development ###

Place your controllers in this directory.

* Your controller function must be 'Generator' function.

`controller.hello = function*() {
    this.body = "Hello World";
};`

## Coding Conventions ##

* add "use strict;" to the top of your JS files
* you can use features ECMA6 JS
* please, just stick with lowercase file names for controllers, eg 'my_controller.js’.
* Please, write your controller methods with camelCase, eg 'myMethod'.

### Testing ###

Place your controller function test to the tests/controllers folder.

***
## Commenting Conventions ##

Commenting should be done as often as possible and following the conventions of [JSDoc][JSDOC]. Good practice is to write out the steps you want to achieve in the comments before you write the code. It is ideal that Issue/Task content is written in point form in such a way it may be copied as comment-steps into the code.

