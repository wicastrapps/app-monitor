$(document).ready(function () {

    $("#loading").show();

    var heightPropagated = false;

    $.get('/views/sysinfo.ejs', function (template) {
        // Compile the EJS template.
        var tplCompiled = ejs.compile(template);

        var refreshInfo = function () {
            // Grab the data
            $.get('/sysinfo', function (data) {
                // Generate the html from the given data.
                $('.sysinfo-container').html(tplCompiled({data: data.info}));
                $("#loading").hide();

                if (!heightPropagated) {
                    heightPropagated = true;
                    
                    propagateHeight();
                }
            });

            setTimeout(refreshInfo, 5000);
        };

        refreshInfo();
    });

    // sends the current height to possible users of the monitor to adjust the display accordingly
    var propagateHeight = function() {
        var height = $('.sys-info').css('height');
        if (window.parent) {
            window.parent.postMessage(height, "*");
        }
    };

    // $(window).on('resize', function(e) {
    //     propagateHeight();
    // });
});