// - - - - - - - - - - - - - - - - projects Router - - - - - - - - - - - - - - - - -  //                                                                                 */
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - //

'use strict';

const Router = require('koa-router');
const monitorRouter = new Router;

const monitorCTRL = require('../controllers/monitor_ctrl');

// get info
monitorRouter.get('/', 
    monitorCTRL.displayInfo
);

module.exports = monitorRouter;