
# WiCastr API Starter routes#

***
### Development ###

Place your routers in this directory.

* Your controller function must be 'Generator' function.
* Add validate(joiScheme) middleware for request data validation.

```javascript
const testSch = require('../validations/hello_world');
const helloController = require('../controllers/hello_world');
const validate = require('../validations/index');
helloRouter.post('/cars', validate(testSch.cars), helloController.testValidation);
```

--------------------------------------------------------
For an easy understanding use this structure for every resource:

    | Resource              GET                     POST                    PUT                         DELETE
                            read                   create                  update

    | /cars         Returns a list of cars 	    Create a new ticket 	Bulk update of cars 	    Delete all cars
    | /cars/711 	Returns a specific car 	    Method not allowed  	Updates a specific ticket 	Deletes a specific ticket


--------------------------------------------------------
Provide filtering, sorting, field selection and paging for collections

Filtering:
Use a unique query parameter for all fields or a query language for filtering.
`GET /cars?color=red` Returns a list of red cars
`GET /cars?seats<=2`  Returns a list of cars with a maximum of 2 seats

--------------------------------------------------------

Sorting:
Allow ascending and descending sorting over multiple fields.

`GET /cars?sort=-manufactorer,+model`

This returns a list of cars sorted by descending manufacturers and ascending models.

--------------------------------------------------------

Field selection

`GET /cars?fields=manufacturer,model,id,color`

--------------------------------------------------------

Paging
Use limit and offset. It is flexible for the user and common in leading databases. The default should be limit=20 and offset=0

`GET /cars?offset=10&limit=5`

## Coding Conventions ##

* add "use strict;" to the top of your JS files
* you can use features ECMA6 JS
* please, just stick with lowercase file names for routes, eg 'my_router.js’.

### Testing ###

You can test endpoints via swagger

***
## Commenting Conventions ##

Commenting should be done as often as possible and following the conventions of [JSDoc][JSDOC]. Good practice is to write out the steps you want to achieve in the comments before you write the code. It is ideal that Issue/Task content is written in point form in such a way it may be copied as comment-steps into the code.
